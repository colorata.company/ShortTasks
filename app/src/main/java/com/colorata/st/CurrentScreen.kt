package com.colorata.st

enum class CurrentScreen {
    POWER,
    ADD_APP,
    THEME_PICKER,
    POWER_CONTROLS
}